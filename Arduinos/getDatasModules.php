<?php
namespace Arduinos;

use App\Autoloader as Autoloader2;
use App\model\buseModel;
use App\model\toShowAlertModel;
use Core\Autoloader;
use Core\Date\gestionDate;
use const ROOT;


define("ROOT", dirname(__DIR__).'/');
require ROOT.'App/autoLoader.php';
require ROOT.'/Core/autoLoader.php';
require ROOT.'Arduinos/autoLoader.php';
    
    
    Autoloader::register();
    Autoloader2::register();

$busesObj =  new buseModel();
$verif = $busesObj->getBuseByIP($_SERVER['REMOTE_ADDR']);


if ($verif){ // if IP is in the Db

    $verif['date'] = gestionDate::dateNow();
  
    foreach ($_POST as $key => $value) {
        $laclass = '\\App\\model\\'.$key.'Model';
         
        $Obj = new $laclass;
        $Obj->insertIntoTable($verif,  $value);
    }
    $alertObj = new toShowAlertModel();
    
    //check if any entry for this Buse_groupe
    $entry = $alertObj->getEntryByGroupe($verif['groupe']);
    
    //if no entry put one
    // 'cause it is juste new data, no need to check if it is important or not
    if($entry == false){ 
        $alertObj->setEntry($verif['groupe'], 1, $verif['id_buse']);
    }
    
}

?>
