<?php
namespace App\Connexion;

use App\model\userModel;
use const ROOT;
/**
 *
 */
class connexionControleur
{
  public $error = "Un problème est survenu";
  
  public function checkConnection(){
    if( isset($_POST["email"]) && isset($_POST["pass"])){
      $testUser = new userModel();
      $userInfos = $testUser->infodUser(trim($_POST["email"]));
      
      if ($userInfos){

        if (password_verify($_POST["pass"], $userInfos->pass)){
          
          $_SESSION['id'] =  $userInfos->id_user;
          $_SESSION['gr'] =  $userInfos->groupe;

          header('Location: index.php');

        }
      }
      }else{
        $this->error = false;
    }
    require ROOT.'App/Connexion/connexion.php';
  }

  public function disconnect(){
    session_destroy();
    header('Location: index.php');
  }
}
 ?>
