<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Connexion</title>
    <link rel="stylesheet" href="css/css.css">
    <link rel="icon" type="image/png" href="/images/favicon.png" />
  </head>
  <body>
  <div class="wrapperConnection">
    <div class="squareAuto">
      <form  action="index.php" method="post">
        <label for="email">votre email:</label>
        <input type="email" id="email" name="email" value="" placeholder="votre email" required>
        <label for="pass">votre mot de passe:</label>
        <input type="password" id="pass" name="pass" value="" placeholder="votre mot de passe" required>
        <button type="submit">connexion</button>
      </form>
    <?php if ($this->error): ?>
        <p class="error"><?php echo $this->error; ?></p>
    <?php endif ?>
    </div>
      <img style="margin: 60px auto; width: auto; max-width: 80%" src="https://i.pinimg.com/originals/d9/46/39/d94639c6d5c406cd28de7e4ff0e9269d.jpg" alt="image greenhouse" />
  </div>
  </body>
</html>
