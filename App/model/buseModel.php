<?php

namespace App\model;

use Core\LaBdd\bdd;
use PDO;

class buseModel extends bdd
{
    /**
     * retrives all board of an user
     *
     * @return array[obj]
     */
    public function getBuses(){
        $sql = $this->getBdd()->prepare("SELECT * FROM buses WHERE groupe=:groupe");
        $sql->setFetchMode(PDO::FETCH_OBJ);
        $sql->bindParam(':groupe', $_SESSION['gr']);
        $sql->execute();
        $rep = $sql->fetchAll();
        return $rep;    
    }

/**
 * retrives last entry in table $quoi
 *
 * @param [str] $quoi on select $quoi
 * @param str $ou la table
 * @param int $id_buse 
 * @return obj
 */
    public function getLastInfo($quoi, $ou, $id_buse){
        $requete = 'SELECT ' . $quoi . ' from '. $ou .' WHERE id_buse=:id_b ORDER BY id DESC LIMIT 1';

        $sql = $this->getBdd()->prepare($requete);
        $sql->bindParam(":id_b", $id_buse, PDO::PARAM_INT);
        $sql->execute();
        $rep = $sql->fetch();
        return $rep; 
    }

    /**
     * used to verify if sender is in the DATABASE
     *
     * @param [] $d
     * @return array
     */
    public  function getBuseByIP($d){
        $sql = $this->getBdd()->prepare("SELECT id_user, buses.id_buse, buses.groupe FROM users JOIN buses ON users.groupe = buses.groupe WHERE buses.adresse=:addr");
        $sql->bindParam(":addr", $d);
        $sql->execute();
        $rep = $sql->fetch(PDO::FETCH_ASSOC);
        return $rep;
    }

    /** 
     * add buse to DB
     * @param str $nom nom
     * @param str $adr adresse IP de la buse
     * @return int id_buse
     */
    public function addBuse($nom, $adr){
        $sql = $this->getBdd()->prepare("INSERT INTO buses (name_buse, adresse, groupe) VALUES ( :nam, :adre, :grp)");
        $sql->bindParam(":nam", $nom);
        $sql->bindParam(":adre", $adr);
        $sql->bindParam(":grp", $_SESSION['gr']);
        $sql->execute();
        $rep = $this->getBdd()->lastInsertId();
        return $rep;
    }


    /** 
     * remove buse
     * 
     * @param int $d id_buse to remove
     * @return bool
     */
    public function removeBuse($d){
        $sql = $this->getBdd()->prepare("DELETE FROM buses WHERE id_buse=:id_b");
        $sql->bindParam(":id_b", $d);
        $rep = $sql->execute();
        return $rep;
    }
}
?>