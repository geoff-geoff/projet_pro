<?php
namespace App\model;

use Core\LaBdd\bdd;
use PDO;

class userModel extends bdd
{

    /**
     * pour check la connexion
     *
     * @param [str/EMAIL]  $d
     * @return OBJ_ano
     */
    public function infodUser($d){
        $sql = $this->getBdd()->prepare("SELECT * FROM users WHERE email=:mail");
        $sql->bindParam("mail", $d, PDO::PARAM_STR);
        $sql->setFetchMode(PDO::FETCH_OBJ);
        $sql->execute();
        $rep = $sql->fetch();
        return $rep;    
    }

    /**
     * get info user from session['id']
     *
     * @param [int] $d
     * @return OBJ_ano
     */
    public function userInfo($d){
        $sql = $this->getBdd()->prepare("SELECT * FROM users WHERE id_user=:id");
        $sql->bindParam(":id", $d, PDO::PARAM_INT);
        $sql->setFetchMode(PDO::FETCH_OBJ);
        $sql->execute();
        $rep = $sql->fetch();
        return $rep;    
    }

    /**
     * return all users
     *
     * @return array[OBJ]
     */
    public function allUsers(){
        $sql = $this->getBdd()->prepare("SELECT * FROM users ORDER BY id_user");
        $sql->setFetchMode(PDO::FETCH_OBJ);
        $sql->execute();
        $rep = $sql->fetchAll();
        return $rep;    
    }
    
    /**
     *  add user to DB
     * 
     * @param str $nom 
     * @param srt $prenom
     * @param str $email
     * @param str pass hashed
     * @param str $token 
     * @param int $groupe
     * @return bool 
     */
    public function addUser($n, $p, $e, $pass, $token){
        $sql = $this->getBdd()->prepare("INSERT INTO users (nom, prenom, email, pass, token, groupe) VALUES (:nom, :prenom, :email, :pass, :tok, :groupe)");
        $sql->bindParam(":nom",$n, PDO::PARAM_STR);
        $sql->bindParam(":prenom", $p, PDO::PARAM_STR);
        $sql->bindParam(":email", $e);
        $sql->bindParam(":pass", $pass);
        $sql->bindParam(":tok", $token);
        $sql->bindParam(":groupe", $_SESSION['gr']);
        $rep = $sql->execute();
        return $rep;
    }

    /** 
     * remove user from Db
     * 
     * @param str $d token
     */
    public function removeUser($d){
        $sql = $this->getBdd()->prepare("DELETE FROM users WHERE token=:tok");
        $sql->bindParam(":tok", $d);
        $rep = $sql->execute();
        return $rep;
    }
    
    public function insertTokenCsrf($param, $id) {
        $sql = $this->getBdd()->prepare("UPDATE users SET token_csrf=:tok WHERE id_user=:id");
        $sql->bindParam(":tok", $param);
        $sql->bindParam(":id", $id);
        $sql->execute();
    }
    
    public function checkTokenCsrf($id) {
        $sql = $this->getBdd()->prepare("SELECT token_csrf FROM users WHERE id_user=:id");
        $sql->bindParam(":id", $id);
        $sql->execute();
        $rep= $sql->fetch();
        return $rep;
    }
}
?>