<?php
namespace App\model;

use Core\LaBdd\bdd;
use PDO;

class temperatureModel extends bdd
{
    /**
     *  add to table
     * 
     * @param array $verif [id_buse, date]
     * @param int valeur a mettre en base
     */
    public function insertIntoTable($verif,$post){
        $sql =  $this->getBdd()->prepare("INSERT INTO temperature (id_buse, dat, temp) VALUES (:id_bu, :da, :para)");
        $sql->bindParam(":id_bu",$verif['id_buse']);
        $sql->bindParam(":da",$verif['date']);
        $sql->bindParam(":para",$post);
        $sql->execute();
    }
    
    /**
     * return MAX(temp) by day from $d
     *  
     * @param date $d
     * @return array of array [max,dates]
     */
    public function getMaxTempFromDates($d){
         $sql =  $this->getBdd()->prepare("SELECT max(temp), to_char( dat, 'YYYY-MM-DD') as dates FROM temperature WHERE temperature.dat > :from GROUP BY dates");
         $sql->bindParam(':from', $d);
         $sql->execute();
         $rep = $sql->fetchAll();
         return $rep;
    }
    
    /**
     * return all MIN(temp) by date from $d
     * 
     * @param date $d
     * @return array of array[min, dates]
     */
    public function getMinTempFromDates($d){
         $sql =  $this->getBdd()->prepare("SELECT min(temp), to_char( dat, 'YYYY-MM-DD') as dates FROM temperature WHERE temperature.dat > :from GROUP BY dates");
         $sql->bindParam(':from', $d);
         $sql->execute();
         $rep = $sql->fetchAll();
         return $rep;
    }
    
    /** 
     * return average temperature
     * 
     * @param date $d date from 
     * @return array of array [round, dates]
     */
    public function getAvgTempFromDate($d){
         $sql =  $this->getBdd()->prepare("SELECT ROUND(AVG(temp), 1), to_char( dat, 'YYYY-MM-DD') as dates FROM temperature WHERE temperature.dat > :from GROUP BY dates");
         $sql->bindParam(':from', $d);
         $sql->execute();
         $rep = $sql->fetchAll();
         return $rep;
    }
    
    
    public function getInfoDay($id, $d){
        
        $da = $d . '%';
        
        $sql =  $this->getBdd()->prepare("SELECT temp FROM temperature WHERE id_buse=:id AND dat::text LIKE :da");
        $sql->bindParam(":id", $id);
        $sql->bindParam(":da", $da);
        $sql->execute();
        $rep = $sql->fetchAll(PDO::FETCH_COLUMN);
        return $rep;
    }
    
    public function graphFrom($id, $dat) {
        $sql =  $this->getBdd()->prepare("SELECT temp, dat FROM temperature WHERE id_buse=:id AND dat::text >= :da");
        $sql->bindParam(":id", $id);
        $sql->bindParam(":da", $dat);
        $sql->execute();
        $rep = $sql->fetchAll(PDO::FETCH_NUM);
        return $rep;
    }
    
    public function graphPeriode($buse, $dateFrom, $dateTo) {
        $sql =  $this->getBdd()->prepare("SELECT temp, dat FROM temperature WHERE id_buse=:id AND dat::text >= :da AND dat::text <=:dt");
        $sql->bindParam(":id", $buse);
        $sql->bindParam(":da", $dateFrom);
        $sql->bindParam(":dt", $dateTo);
        $sql->execute();
        $rep = $sql->fetchAll(PDO::FETCH_NUM);
        return $rep;
    }
}
?>