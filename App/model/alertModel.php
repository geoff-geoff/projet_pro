<?php
namespace App\model;

use Core\LaBdd\bdd;

class alertModel extends bdd
{
/**
 * Get last 5 errors
 * 
 * @return array of array
 */
    public function last5Alerts(){
        $sql = $this->getBdd()->prepare("SELECT id_alert, name_buse, date_alert FROM sent_alert Join buses ON sent_alert.id_buse = buses.id_buse WHERE buses.groupe = :groupe ORDER BY sent_alert.id DESC LIMIT 5");
        $sql->bindParam(":groupe", $_SESSION['gr']);
        $sql->execute();
        $rep = $sql->fetchAll();
        return $rep;
    }
}
?>