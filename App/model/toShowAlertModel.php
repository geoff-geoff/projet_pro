<?php
namespace App\model;

use Core\LaBdd\bdd;
use PDO;


class toShowAlertModel extends bdd {
  
    public function getEntryByGroupe($gr){
        $sql = $this->getBdd()->prepare("SELECT niv_alert, buse, groupe FROM to_show WHERE groupe=:gr");
        $sql->bindParam(":gr", $gr);
        $sql->execute();
        $rep = $sql->fetch(PDO::FETCH_ASSOC);
        return $rep;
    }
    
    public function setEntry($gr, $niv, $buse){
        $sql = $this->getBdd()->prepare("INSERT INTO to_show (groupe, niv_alert, buse) VALUES (:gr, :niv, :buse)");
        $sql->bindParam(":gr", $gr);
        $sql->bindParam(":niv", $niv);
        $sql->bindParam(":buse", $buse);
        $sql->execute();
    }
}
?>