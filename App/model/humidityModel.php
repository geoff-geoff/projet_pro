<?php
namespace App\model;

use Core\LaBdd\bdd;
use PDO;

class humidityModel extends bdd
{
    /**
     *  add to table
     * 
     * @param array $verif [id_buse, date]
     * @param int valeur a mettre en base
     */
    public function insertIntoTable($verif,$post){
        $sql =  $this->getBdd()->prepare("INSERT INTO humidity (id_buse, dat, taux) VALUES (:id_bu, :da, :para)");
        $sql->bindParam(":id_bu",$verif['id_buse']);
        $sql->bindParam(":da",$verif['date']);
        $sql->bindParam(":para",$post);
        $sql->execute();

    }
    
       public function getInfoDay($id, $d){
        
        $da = $d . '%';
        
        $sql =  $this->getBdd()->prepare("SELECT taux FROM humidity WHERE id_buse=:id AND dat::text LIKE :da");
        $sql->bindParam(":id", $id);
        $sql->bindParam(":da", $da);
        $sql->execute();
        $rep = $sql->fetchAll(PDO::FETCH_COLUMN);
        return $rep;
    }
    
    public function graphFrom($id, $dat) {
        $sql =  $this->getBdd()->prepare("SELECT taux, dat FROM humidity WHERE id_buse=:id AND dat::text >= :da");
        $sql->bindParam(":id", $id);
        $sql->bindParam(":da", $dat);
        $sql->execute();
        $rep = $sql->fetchAll(PDO::FETCH_NUM);
        return $rep;
    }
    
    public function graphPeriode($buse, $dateFrom, $dateTo) {
        $sql =  $this->getBdd()->prepare("SELECT taux, dat FROM humidity WHERE id_buse=:id AND dat::text >= :da AND dat::text <=:dt");
        $sql->bindParam(":id", $buse);
        $sql->bindParam(":da", $dateFrom);
        $sql->bindParam(":dt", $dateTo);
        $sql->execute();
        $rep = $sql->fetchAll(PDO::FETCH_NUM);
        return $rep;
    }
}
?>