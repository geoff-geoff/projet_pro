<?php use Core\Date\gestionDate; ?>

<section class="alerts squareAlerte">
    <p class="bold">Historique des alertes</p>
    <?php foreach ($infos['alerts'] as  $value): ?>
    <div class="squareAlerte whiteBackground">
        <p class="bold"><?php echo $value['id_alert']; ?> voila les limaces</p>
        <p class="italik">le <?php echo gestionDate::dateFromSql($value['date_alert']); ?> sur le module: <span class="bold"><?php echo $value['name_buse'];?></span></p>
        </div>
    <?php endforeach ?>
    
</section>