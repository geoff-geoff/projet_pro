<section class="lastinfos squareAlerte">
    <h3>Les dernières infos collectées :</h3>
    <?php foreach ($infos['modules'] as $module): ?>
    <div class="blockinfos">
        <div class="nomModule">
        <p class="bold"><?php echo $module['nom']; ?>: </p>
        </div>
        <div class="infosModules">
    <?php if($module["vent"]): ?> 
        <div data-element="vent" data-module="<?php echo $module['nom']; ?>" class="ckickable infos square whiteBackground">
            <img src="images/wind.svg" alt="icon vent" class="petiteImage vent"/>
            <p class="italik">vitesse vent</p>
            <?php echo $module["vent"]; ?> nœuds
        </div>
    <?php endif; ?>
     <?php if($module["eau"]): ?>
        <div data-element="eau" data-module="<?php echo $module['nom']; ?>" class="ckickable infos square whiteBackground">
            <img src="images/cuve.svg" alt="icon cuve" class="petiteImage eau"/>
            <p class="italik">cuve</p>
            <?php echo $module["eau"]; ?> Litre<?php if ($module["eau"] > 1) { echo "s";} ?>
        </div>
    <?php endif; ?>
     <?php if($module["humidity"]): ?>
        <div class="ckickable infos square whiteBackground" data-element="humidity" data-module="<?php echo $module['nom']; ?>">
            <img src="images/humidity.svg" alt="icon humidity" class="petiteImage humidity"/>
            <p class="italik">humidité</p>
            <?php echo $module["humidity"]; ?> %
        </div>
    <?php endif; ?>
     <?php if($module["temperature"]): ?>
        <div class="ckickable infos square whiteBackground" data-element="temperature" data-module="<?php echo $module['nom']; ?>">
            <img src="images/temp.svg" alt="icon temp" class="petiteImage temp" />
            <p class="italik">température</p>
            <?php echo $module["temperature"]; ?> °C
        </div>
    <?php endif; ?>
        </div>
    </div>
        <?php endforeach ?>
</section>
<section class="squareAlerte">
    <h3>graph of the day</h3>
    <!--TODO all have to be into the JS-->
    <svg class="squareAuto" id="graph" viewBox="0 0 500 100"> 
        <g>
        <polyline fill="none" points="5,1 5,99" stroke="#542123" stroke-width="0.5"></polyline> 
        <text class="temperatureMax" x="8" y="5"></text>
        <text class="temperatureMin" x="8" y="95"></text>
        <text class="humidityMax" x="1" y="5"></text>
        <text class="humidityMin" x="1" y="95"></text>
        <text class="eauMax" x="489" y="5"></text>
        <text class="eauMin" x="489" y="95"></text>
        <text class="ventMax" x="492" y="10"></text>
        <text class="ventMin" x="492" y="95"></text>
        </g>
    </svg>
</section>
