<section class="modules squareAlerte">
    <h3>Les Modules:</h3>
    <div id="ladate" class="hidden"><?php echo $infos['ladate']; ?></div>
    <div id="listModules" class="listElements">
        <?php foreach ($infos['buses'] as $buse) : ?>
        <div class="square">
            <p class="bold bandeau"><?php echo $buse->name_buse; ?></p>
            <p class="italik">adresse: <?php echo $buse->adresse; ?></p>
                <button class="rmBuse" data-info="<?php echo $buse->id_buse; ?>">suppr</button>
        </div>
        <?php endforeach ?>
    </div>
        <img id="iconAddBuse" class="petiteImage"src="/images/plus.svg" />
    
    <div id="ajoutBuse" class="hidden backgrounder">
        <div class="squareAuto whiteText">
            <h4>ajout module</h4>
            <img id="closeBuse" class="petiteImage hautDroite" src="images/x-button.svg" />
            <label for="nameBuse">son nom:</label>
            <input type="text" id="nameBuse" name="name_buse" placeholder="le nom" required>
            <label for="adresseBuse">son adresse: </label>
            <input type="text" id="adresseBuse" name="adresse" placeholder="adresse">
            <button id="addBuse">ok</button>
        </div>
    </div>
</section>