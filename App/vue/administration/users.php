<section class="users squareAlerte">
    <h3 id="titre">Les utilisateurs</h3>
    <div id="listUsers" class="listElements">
        <?php foreach ($infos['users'] as  $user) : ?>
        <div class="square">
            <p class="bold bandeau"><?php echo ucfirst($user->prenom); ?> <?php echo ucfirst($user->nom); ?></p>
            <p class="italik"><?php echo $user->email; ?></p>
            <?php if ($user->id_user  != $_SESSION['id']): ?>
                <button class="userless" data-info="<?php echo $user->token; ?>">suppr</button>
            <?php endif ?>
        </div>
        <?php endforeach ?>
    </div>
    <img id="iconAddUser" class="petiteImage"src="/images/plus.svg" />
    <div id="addUser" class="hidden backgrounder">
        <div id="ajoutUser" class="squareAuto whiteText">
            <h4>ajout user</h4>
            <img id="closeUser" class="petiteImage hautDroite" src="images/x-button.svg" />
            <label for="name">son nom:</label>
            <input type="text" id="name" name="nom" placeholder="le nom" required>
            <label for="name">son prénom:</label>
            <input type="text" id="prenom" name="prenom" placeholder="le prénom" required>
            <label for="adressemail">adresse email</label>
            <input type="email" id="adressemail" name="email" placeholder="email" required>
            <label for="pass">mot de passe:</label>
            <input type="password" id="pass" placeholder="mot de passe" required>
            <label for="pass2">password, pour la vérif :</label>
            <input type="password" id="pass2" name="pass" placeholder="re le mot de passe" required>
            <button id="adduser">ok</button>
        </div>
    </div>
</section>