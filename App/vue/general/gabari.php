<?php require 'head.php'; ?>
<body>
<div class="wrapper">
<header>
        <div class="menu menu_top">
        <img class="menu-icon" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hamburger_icon.svg/220px-Hamburger_icon.svg.png" alt="menu icon">
        <a class="logo" href="index"><img  src="images/icon.png" alt="icon site"></a>
        </div>
        <nav class="menu ">
          <ul class="menu-list">
            <li><a class="bold" href="index.php">accueil</a></li>
            <li><a class="bold" href="?p=charts">graphiques</a></li>
            <li><a class="bold" href="#">commandes</a></li>
            <li><a class="bold" href="?p=administration">administration</a></li>
            <li><a class="bold" href="?disconnect=true">disconnect</a></li>
          </ul>
      </nav>
</header>
<main class="corp">
  <?php echo $this->corp; ?>
</main>
    <?php require 'foot.php'; ?>