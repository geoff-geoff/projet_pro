<section class="squareAlerte">
    <div class="selecteurs">
        <div class="lescombobox">
            <label>select le module de votre choix</label>
            <select id="buse" class="combobox">
                <option value=""> -- </option>
                <?php foreach($infos['buses'] as $buse): ?>
                <option value="<?php echo $buse->id_buse ?>"><?php echo $buse->name_buse ?></option>
                <?php endforeach; ?>
            </select>
            <label>une durée</label>
            <select id="duree" class="combobox">
                <option value="1_week">1 semaine</option>
                <option value="1_month">1 mois</option>
                <option value="3_month">3 mois</option>
                <option value="6_month">6 mois</option>
                <option value="1_year">1 an</option>
            </select>
            <p>une période, </p>
            du <input id="dateFrom" type="date" />
            au <input id="dateTo" type="date" />
        </div>
        <div class="lesradiobox">
            <input type="checkbox" name="temp" checked="true">Temperatures<br>
            <input type="checkbox" name="wind">vent<br>
            <input type="checkbox" name="humid">humidité<br>
            <input type="checkbox" name="water">cuve<br>
        </div>
    </div>
    <div class="lesGraphs">
        <div id="water" class="hidden squareAlerte whiteBackground">
            <h4>Cuve</h4>
            <svg id="eau"  viewBox="0 0 500 100"></svg>
        </div>    

        <div id="wind" class="hidden squareAlerte whiteBackground">
            <h4>Vent</h4>
            <svg id="vent"  viewBox="0 0 500 100"></svg>
        </div>
        
        <div id="humid" class="hidden squareAlerte whiteBackground">
            <h4>humidité</h4>
            <svg id="humidity"  viewBox="0 0 500 100"></svg>
        </div>    
        
        <div id="temp" class="squareAlerte whiteBackground">
            <h4>température</h4>
            <svg id="temperature"  viewBox="0 0 500 100"></svg>
        </div>    
    </div>
</section>
