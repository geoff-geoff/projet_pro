<?php

namespace App\controleur;

use App\model\buseModel;
use App\model\userModel;
use Core\Date\gestionDate;

/**
 * manage all $_POST
 *
 * @author geoff
 */
class postControlleur {
    
    /** 
     * remove user
     * 
     * @param str token(20)
     * @return bool
     */
    public function removeUser($d){
        if ($this->checkToken($d->tok)){
            $userobj = new userModel();
            $rep = $userobj->removeUser($d->val);
            echo $rep;
        }
    }
    
    
    /** 
     * add user to group connected
     * 
     * @param obj [nom,prenom,adressemail,pass]
     * @return str token(20)
     */
    public function createUser($d) {
        if ($this->checkToken($d->tok)){
            $userobj = new userModel();
        
            $n = htmlspecialchars($d->nom);
            $p = htmlspecialchars($d->prenom);
            $e = filter_var(trim($d->adressemail), FILTER_VALIDATE_EMAIL);
            $pass = password_hash($d->pass, PASSWORD_DEFAULT);

            if ($e && isset($n) && isset($p) && isset($pass)) {
                $token = bin2hex(random_bytes(15));
                $rep = $userobj->addUser($n, $p, $e, $pass, $token);
                if ($rep){
                    echo $token;  // AJAX answer
                }
            }
        }
    }
    
    /** 
     * add Buse to database
     * 
     * @param obj [nom, adresse]
     * @return int id buse
     */
    public function addBuse($d){
        if ($this->checkToken($d->tok)){
            $buseObj = new buseModel();

            $nm = htmlspecialchars($d->nom);
            $adr = htmlspecialchars(trim($d->adresse));

            $rep = $buseObj->addBuse($nm, $adr);
            echo $rep; // answer AJAX
        }
    }
    
    /**
     *  remove Buse
     * 
     * @param int id buse to remove
     * @return bool ok/false
     */
    public function removeBuse($d){
        if ($this->checkToken($d->tok)){
            $userobj = new buseModel();
            $rep = $userobj->removeBuse($d->val);
            echo $rep;
        }
    }
    
    public function getGraphs($d){
        $tables = ["humidity", "eau", "vent", "temperature"];
        
        [$a, $fu] = explode("_", $d->duree);
        $fu .= 'Before';
        $periode = gestionDate::$fu($a);
        
        $rep = [];
        
        foreach ($tables as $tab){
            $t = '\\App\\model\\'.$tab.'Model';
            $obj = new $t();
            
            $rep[$tab] = $obj->graphFrom($d->buse, $periode);
        }
        echo json_encode($rep);
        
    }
    public function getGraphsPeriode($d) {
        $tables = ["humidity", "eau", "vent", "temperature"];
        
        if ($d->dateTo === "") {
            $d->dateTo = gestionDate::tomorrow($d->dateFrom);
        }
        
        $rep = [];
        
        foreach ($tables as $tab){
            $t = '\\App\\model\\'.$tab.'Model';
            $obj = new $t();
            
            $rep[$tab] = $obj->graphPeriode($d->buse, $d->dateFrom, $d->dateTo);
        }
        echo json_encode($rep);
    }
    /**
     * check if var === token for this user
     * check against CSRF
     * 
     * @param str $param
     * @return bool
     */
    public function checkToken($param) :bool {
        $userobj = new userModel();
        $verif = $userobj->checkTokenCsrf($_SESSION['id']);
        return $verif['token_csrf'] === $param ? true : false;
    }
}
