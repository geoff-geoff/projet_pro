<?php
namespace App\controleur;


class makePages
{
  private $corp = "";
  
    public function makePage($infos,$d="default"){

        switch ($d) {
            case 'administration':
               
                ob_start();
                require ROOT.'App/vue/administration/users.php';
                $this->corp .= ob_get_clean();  

                ob_start();
                require ROOT.'App/vue/administration/modules.php';
                $this->corp .= ob_get_clean();
                require ROOT.'App/vue/general/gabari.php';
                break;

            case 'charts':
                
                ob_start();
                require ROOT.'App/vue/graph/charts.php';
                $this->corp = ob_get_clean();
                require ROOT.'App/vue/general/gabari.php';
                break;
            
            case '404':
                ob_start();
                require ROOT.'App/vue/404Vue.php';
                $this->corp = ob_get_clean();
                require ROOT.'App/vue/general/gabari.php';
                break;
            
            default:
                ob_start();
                 require ROOT.'App/vue/indexPage/lastInfos.php';
                $this->corp .= ob_get_clean();
                
                if (count($infos['alerts']) > 0){
                    ob_start();
                    require ROOT. 'App/vue/indexPage/alerts.php';
                    $this->corp .= ob_get_clean();
                }


                require ROOT.'App/vue/general/gabari.php';
                break;
        }
    }
}


?>