<?php
namespace App\controleur;

use App\controleur\makePages;
use App\model\buseModel;
use App\model\userModel;
use Core\Date\gestionDate;


class administrationControleur
{
    const  PAGE = "administration";
    private $user;
    private $lesInfos;
    private $userobj;

    public function __construct(){
        $this->userobj = new userModel();
        $this->user = $this->userobj->userInfo($_SESSION['id']);
    }

    /**
     * all PRIMARY infos to make the page
     *
     * run all other functions
     */
    public function setInfos(){
        if (isset($_GET['action'])){
            $this->manageActions($_GET['action']);
        }
        
        $this->lesInfos["userPrenom"] = ucfirst($this->user->prenom);
        $this->lesInfos["title"] = "Administration";
        $this->lesInfos["script"] = ["administration.js", "js.js"];
        $this->lesInfos["buses"] = $this->getBuses();
        $this->lesInfos['users'] = $this->getUsers();
        
        $this->createLaPage();
    }
    
    /**
     * getBuses
     *
     * @return array
     */
    private function getBuses(){
        $buseObj = new buseModel();
        return $buseObj->getBuses();
    }

    /**
     * render Page Always last
     *
     * set token_csrf to check if really comming from this user.
     */
    private function createLaPage(){ 
        $token = gestionDate::dateNow();
        $this->userobj->insertTokenCsrf($token, $_SESSION['id']);
        
        $this->lesInfos['ladate'] = $token;
        
        $pages = new makePages();
        $pages->makePage($this->lesInfos, self::PAGE);
    }

    private function getUsers(){
        return $this->userobj->allUsers();
    }    
}


?>