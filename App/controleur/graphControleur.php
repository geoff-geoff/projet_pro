<?php

namespace App\controleur;

use Core\Date\gestionDate;
use App\model\buseModel;
use App\controleur\makePages;

/**
 * controlleur pour les traitement graph
 *
 * @author geoff
 */
class graphControleur {
    
    const  PAGE = "charts";
    
    private $buses;  // toutes les buses de User
    private $lesInfos;  // les infos a passer pour la construction de la page.

    
    public function __construct() {
        $obj = new buseModel();
        $this->buses = $obj->getBuses();
    }
    
    public function graphOfTheDay($d){
        $today = gestionDate::dateToday();
        $tomorrow = gestionDate::tomorrow($today);
        foreach ($this->buses as $buse) {
            if ($buse->name_buse === $d->buse){
                $buseId = $buse->id_buse; 
            }
        }
        $laclass = 'App\\model\\'.$d->element . "Model";
        $obj = new $laclass;
        $rep = $obj->graphPeriode($buseId, $today, $tomorrow);
//        $rep[count($rep)] = $d->element;
        echo json_encode($rep);
    }
    
    public function setInfos() {
        $this->lesInfos['title'] = "les graphiques";
        $this->lesInfos['buses'] = $this->buses;
        $this->lesInfos["script"] = ["graph.js","biblio.js", "chartspage.js", "js.js"];
        $obj = new makePages();
        $obj->makePage($this->lesInfos, self::PAGE);
    }
}