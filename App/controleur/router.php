<?php
namespace App\controleur;

use App\Connexion\connexionControleur;
use App\controleur\administrationControleur;
use App\controleur\errorControlleur;
use App\controleur\indexPage;
use App\controleur\postControlleur;


class router
{
    private $pages;  // obj  makePage

    
    public function __construct(){
        $this->pages =  new makePages();
    }
  /**
   * gére la page de connexion, 
   */
  public function connection(){
    $connetion = new connexionControleur();
    $connetion->checkConnection();
  }

  public function disconnect(){
    $connetion = new connexionControleur();
    $connetion->disconnect();
  }


  /**
   * redirection suivant les pages
   *
   */
  public function router(){
    if (isset($_GET['p'])){   // P stand for Page

        switch ($_GET['p']) {
            case 'administration':
                $obj = new administrationControleur();
                $obj->setInfos();
                break;
            
            case 'charts':
                $obj = new graphControleur();
                $obj->setInfos();
                break;
            
            default:
                $obj = new errorControleur();
                $obj->setInfos();
                break;
            }

        } else {
            $obj = new indexPage();
            $obj->setInfos();
        }  // END IF/ELSE
  }

 /** 
  * manage les POST
  * 
  */
    public function lesPost() {
        $obj = new postControlleur();
        $objGraph = new graphControleur();
        
        if (isset($_POST["userless"])){
            $obj->removeUser(json_decode($_POST["userless"]));
        }
        if(isset($_POST['adduser'])){  // objs receved
             $payload = json_decode($_POST["adduser"]);
             $obj->createUser($payload);
        }
        if (isset($_POST['graphIndex'])){  // obj receved
            $payload = json_decode($_POST["graphIndex"]);
            $objGraph->graphOfTheDay($payload);
        }
        if (isset($_POST['newbuse'])){   // obj receved
            $payload = json_decode($_POST["newbuse"]);
            $obj->addBuse($payload);
        }
        if (isset($_POST['rmBuse'])){
            $obj->removeBuse(json_decode($_POST["rmBuse"]));
        }
        if(isset($_POST['graphPage'])){
            $payload = json_decode($_POST['graphPage']);
            $obj->getGraphs($payload);
        }
        if(isset($_POST['graphPagePeriode'])){
            $payload = json_decode($_POST['graphPagePeriode']);
            $obj->getGraphsPeriode($payload);
        }
    }
// END router()


}
 ?>
