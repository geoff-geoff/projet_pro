<?php
namespace App\controleur;

use App\controleur\makePages;
use App\model\userModel;
use App\model\buseModel;
use App\model\alertModel;


class indexPage
{
    private $user;  // récup prenom
    private $lesInfos;  // les infos à transmettre pour generer la page.
    
    public function __construct(){
        $userobj = new userModel();
        $this->user = $userobj->userInfo($_SESSION['id']);

    }
    /** 
     * main function, start all the others
     * 
     */
    public function setInfos(){
        $this->lesInfos["userPrenom"] = ucfirst($this->user->prenom);
        $this->lesInfos["title"] = "Bonjour";
        $this->lesInfos["script"] = ["graph.js", "infosToGraph.js" , "js.js"];
        $this->getLastInfos();
        $this->getLastalerts();


        $this->createLaPage(); // to generate the page  HAVE TO BE THE LAST ONE
    }
    /** 
     * send datas to makePage
     * 
     */
    private function createLaPage(){
        
        $pages = new makePages();
        $pages->makePage($this->lesInfos);
    }


    /**
    * retrives last infos for 
    * all boards of the user groupe.
    *
    * @return ARrAy of ARRAY
    */
    private function getLastInfos(){
        $buseObj = new buseModel();
        $ouquoi = ["vent" => 'vitesse',
                            "eau" => "volume",
                            "temperature" => "temp",
                            "humidity" => "taux",];

        $lesModules = $buseObj->getBuses();
 
        $modules = [];         //to stock all list
        foreach ($lesModules as $module) {
            $a ["nom"] = $module->name_buse;
            
            foreach ($ouquoi as $ou => $quoi) {
                $info = $buseObj->getLastInfo($quoi, $ou, $module->id_buse);
               $a[$ou] = $info[$quoi];
            }
            array_push($modules, $a);
        }
        $this->lesInfos['modules'] = $modules;
    }

    /**
     * return last 5 alerts
     *
     * @return array
     */
    private function getLastalerts(){

        $alertObj = new alertModel();
        $this->lesInfos['alerts'] = $alertObj->last5Alerts();
    }
}
?>