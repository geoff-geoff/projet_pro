<?php 
namespace Core\Date;

use date;
use DateTime;
use explode;

class gestionDate
{
    /** 
     * from YYYY-m-d to d / m / YYY
     * 
     * @param date like YYYY-m-d H:i:s
     * @return str 
     */
    public static function dateFromSql($d){
        list($da,) = explode(' ', $d);
        $r = explode('-', $da);
        return $r["2"].' / '.$r['1'].' / '.$r["0"];
    }
    
    /** 
     * datenow
     * @return str "Y-m-d H:i:s"
     */
    public static function dateNow(){
        return  date("Y-m-d H:i:s");
    }
    
    /** 
     * 
     * @return str date  "Y-m-d"
     */
    public static function dateToday(){
        return date("Y-m-d");
    }
    
    public static function tomorrow($d){
        $datetime = new DateTime($d);
        $datetime->modify('+1 day');
        return $datetime->format('Y-m-d');
    }

    /** 
     * return the date of Last week
     *    
     *@return date YYYY-m-d
      */
    public static function weekBefore($d = 1){
        return date("Y-m-d" , mktime(0, 0, 0,  date("m"), date("d")-(7 * $d), date("Y")));
    }
      
    /** 
     * return date of X month before
     * 
     * @param int $d Default at 1
     * @return date YYYY-m-d
     */
    public static function monthBefore($d = 1){
        return date("Y-m-d" , mktime(0, 0, 0,  date("m") - $d, date("d"), date("Y")));
    }
    
    /**
     *  return date of X years before
     * @param int $d Default = 1
     * @return date YYYY-m-d
     */
    public static function yearBefore($d=1){
        return date("Y-m-d" , mktime(0, 0, 0, date("m"), date("d"), date("Y") -$d));
    }
}
