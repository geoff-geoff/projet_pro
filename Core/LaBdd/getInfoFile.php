<?php
namespace Core\LaBdd;


/* *
*Singleton pour récup les info de la connection pour la Base
*/

class getInfoFile
{

    private $config;
    private static $_instance;
    

    public function __construct(){
        $this->config = parse_ini_file(ROOT.".conf");
    }
  

    /**
     * creation du Singleton si non existant
     *
     * @return instance de self
     */
    public static function dataBaseInfoInstance(){
        if (self::$_instance == null ){
            self::$_instance = new getInfoFile();
        }
        return self::$_instance;
            }

  /**
     * return database info from .config file
     * @return array
     */
    public function getInfos(){
        return $this->config;
    }
}

?>