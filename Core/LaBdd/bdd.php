<?php
namespace Core\LaBdd;

use Core\LaBdd\getInfoFile;
use PDO;


class bdd
{
    private $bdd;
    private $infodb;

    public function __construct(){
        $this->setinfo();
    }

    protected function getBdd(){
        if ($this->infodb['driver'] === "pgsql"){
            $this->postgreDbCall();
        }
        if ($this->infodb['driver'] === "mysql"){
            $this->mysqlDbCall();
        }
        
        return $this->bdd;
    }
    // TODO have to test that !!!!
    private function mysqlDbCall(){
        if ($this->bdd == null){
      $this->bdd = new PDO('mysql:host='.$this->infodb['host'].';dbname='.$this->infodb['dbname'].';charset=utf8', $this->infodb['user'], $this->infodb['pass']);
        }
    }
    
    /**
     * 
     */
    private function postgreDbCall(){
        if ($this->bdd == null){
          $this->bdd = new PDO($this->infodb['driver'].':host='.$this->infodb['host'] .' ;dbname=' . $this->infodb['dbname'] .';user='. $this->infodb['user'].';password='. $this->infodb['pass']);
        }
    }

    /**
     * on récup les info du fichier .conf  via un Singleton
     *
     * @return array
     */
    private function setinfo(){
          $this->infodb = getInfoFile::dataBaseInfoInstance()->getInfos();         
      }
}


?>