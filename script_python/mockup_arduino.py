#!/usr/bin/python3

import requests
import json
from random import randrange

payload={}
url = "http://192.168.3.125/geoff/projet_pro/Arduinos/getDatasModules.php" 

payload["eau"] = randrange(1200)
payload["temperature"] = randrange(-10,40)
payload["humidity"] = randrange(100)
payload["vent"] = randrange(120)



# pour le moment on test, rien d'autre
#print(payload)

# sortie test 1
# {'eau': 196, 'temperature': 14, 'humidity': 39, 'vent': 58}

#ici on envoit en post
r = requests.post(url, data=payload)

#debug requette
#print(r.text)