MENU = {

    taille: "300px",
    divAopen : document.querySelector('.menu-list'),
  
    INIT() {
      document.querySelector('.menu-icon').addEventListener("click", MENU.openClose);
    },
    openClose(){
      MENU.divAopen.style.height === MENU.taille ? MENU.divAopen.style.height = "0" : MENU.divAopen.style.height = MENU.taille;
    }
  };
  MENU.INIT();