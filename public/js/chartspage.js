CHARTSPAGE = {
    
    buse: "",
    duree: "",
    element: "",
    
    INIT(){
        document.querySelectorAll("input[type='checkbox']").forEach(a => a.addEventListener('change', CHARTSPAGE.toggleGraph));
        document.querySelectorAll(".combobox").forEach(a => a.addEventListener('change', CHARTSPAGE.sendRequest));
        document.querySelectorAll("input[type='date']").forEach(a => a.addEventListener('change', CHARTSPAGE.sendRequestDate));
    },
    
    /**
     * toggle class="hidden"
     * 
     * @param {obj} e
     */
    toggleGraph(e){
        toToggle = document.querySelector("#" + e.target.name).classList.toggle('hidden');
    },
    /**
     * récup id buse, periode
     * envois objet en ajax
     * 
     */
    sendRequest(){
        let what = {
            buse : document.querySelector("#buse").value,
            duree : document.querySelector('#duree').value
        };
        AJAX.Post(what,"index.php", "graphPage=", CHARTSPAGE.postPost);
    },
    /**
     * récup buse_id, date début, date fin
     * envois par Ajax
     *
     */
    sendRequestDate(){
      let what = {
          buse : document.querySelector("#buse").value,
          dateFrom: document.querySelector('#dateFrom').value,
          dateTo: document.querySelector('#dateTo').value
      };
        console.log(what);
      AJAX.Post(what,"index.php", "graphPagePeriode=", CHARTSPAGE.postPost);
    },
    
    /**
     * 
     * Post Ajax, callback pour création des graphs
     * clean vielles courbes et insert les nouvelles.
     * @param {obj} e  event Ajax.
     * 
     */
    postPost(e){
        const rep = JSON.parse(e);
        const keys = Object.keys(rep);
        keys.forEach(a => {CHARTSPAGE.element = a,
                            CHARTSPAGE.delChild(document.querySelector("#"+a)),
                           document.querySelector("#"+a).appendChild(CHARTSPAGE.splitToGraph(rep[a], document.querySelector("#"+a)));
                           });
    },
    
    /**
     * 
     * @param {obj} ab
     * @param {elem} cadreSVG
     * @returns {elem} <g> insert to graph
     */
    splitToGraph(ab, cadreSVG){
        const dates = [];
        const values = [];
        ab.forEach(a => {
                    [v,d] = a,
                    dates.push(d),
                    values.push(v);
        });
       
        const reponse = GRAPH.INIT(values,dates,CHARTSPAGE.element, cadreSVG);
        return reponse;
    },
    
    
    /**
     * check if already a <g> in the <svg>
     * 
     * @param {node} a  id svg
     * 
     */
    delChild(a){
        a.children.length > 0 ? a.removeChild(a.children[0]) : false;
    }
};
CHARTSPAGE.INIT();