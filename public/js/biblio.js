AJAX = {
  xhr: "",
  changedToJson : false,

/**
 * send POST to server
 * 
 *@param {str} what payload
 *@param {str} to   adresse de destination
 *@param {str} attributs key for PHP 
 *@param {str} whatToDo callback
 */
  Post(what, to, attributs,whatToDo){
        
    AJAX.xhr = new XMLHttpRequest(),

    AJAX.xhr.onload = function(){
      if (AJAX.changedToJson) {
        whatToDo(AJAX.FromJson(AJAX.xhr.responseText));
      } else{
        whatToDo(AJAX.xhr.responseText);
      }
    }; // end ONLOAD
    
    if (typeof what === 'object') {
      what = AJAX.ToJson(what);
    }
    
    AJAX.xhr.open('POST', to , true);
    AJAX.xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    AJAX.xhr.send(attributs+what);
  },
  
  ToJson(togo){
    return JSON.stringify(togo);
  },

  FromJson(tog) {
    try {
      return JSON.parse(tog);
    } catch (e) {
      return tog;
    }
  }
};