GRAPH = {
    
    ns: 'http://www.w3.org/2000/svg', // pour pouvoir créer un des elements SVG avec createelementNS(), address pour récup le xml.
    elem: "", // elem pour couleur ligne graph, position pour legende
    points: "",
    dataValues: "", // les valeurs renvoyé
    coord: "", // str de coord "x,y x,y ..."
    colors: {
        eau: "#0074d9",
        vent: "#237565",
        temperature: 'tomato',
        humidity: "#852145"
    },
    svgcadre: "",

    groupG: "",
    
    /**
     * calcule coordonnées des points pour le graph
     * 
     * @param {array} a liste values
     * @return {str} "x,y x,y ...."
     */
    TransformToCoord(a) {
        const numbPoint = a.length;
        
        // get min & max to calculate Y-axis ratio
        let minus = Math.min(...a);
        const maxus = Math.max(...a);
        
        if (minus > 0) {
            minus = 0;
        }
        
        const ampli = maxus - minus;


        GRAPH.creationLegende(minus, maxus); // appel function pour placer la legende
        
        GRAPH.createBackgroundLines();

        // -------------  x-axis     ---------------  //
        const ratio = (GRAPH.svgcadre.viewBox.baseVal.width - 20) / numbPoint;  // distance between points on x-axis
        let x = 20 - ratio;   // start from left    
        let out = ""; // list point str

        for (let i = 0; i < numbPoint; i++) {
            out += `${x += ratio},${GRAPH.calculeY(a[i], ampli, minus)} `;
        }
        return out;
    },
    
       /** 
     * return position sur l'axe des Y
     * 
     *  @param {int} a valeur
     *  @param {int} amp amplitude de Y-axis
     *  @param {int} minus smaller value
     */
    calculeY(a, amp, minus=0) {
        if (a === 0){
            a = 0.01;
        }
        let result;
        const amplitude_Y = GRAPH.svgcadre.viewBox.baseVal.height + minus ;
        
        a > 0 ? result = amplitude_Y - ((a * 98 / amp)) : result = amplitude_Y + ((a * 98 / minus)  / amp);
        return result;
    },
    
    createPolyline() {
        let poli = document.createElementNS(GRAPH.ns, "polyline");  // création de la ligne avec NameSpace de la W3C
        poli.setAttribute("fill", "none");
        poli.setAttribute("points", GRAPH.coord);
        poli.setAttribute("stroke", GRAPH.colors[GRAPH.elem]);
        poli.setAttribute("stroke-width", "0.3");

        GRAPH.groupG.appendChild(poli);

    },
    
    creationLegende(mi,ma){
        let textmin = document.createElementNS(GRAPH.ns, "text");
        textmin.setAttribute("x", 3);
        textmin.setAttribute("y", 98);
        textmin.innerHTML = mi;
        
        let textmax = document.createElementNS(GRAPH.ns, "text");
        textmax.setAttribute("x", 3);
        textmax.setAttribute("y", 5);
        textmax.innerHTML = ma;
        
        GRAPH.groupG.appendChild(textmin);
        GRAPH.groupG.appendChild(textmax);
    },
    
    createBackgroundLines(){
        const dist = [0.25, 0.5, 0.75];
        for (let i = 0; i < dist.length; i++) {
            
            let line = document.createElementNS(GRAPH.ns, "line");
            line.setAttribute("x1", 3);
            line.setAttribute("y1",`${GRAPH.svgcadre.viewBox.baseVal.height * dist[i]}`);
            line.setAttribute("x2", GRAPH.svgcadre.viewBox.baseVal.width );
            line.setAttribute("y2", `${GRAPH.svgcadre.viewBox.baseVal.height * dist[i]}`);
        
            line.setAttribute("stroke", "LightGray");
            GRAPH.groupG.appendChild(line);
        }
    },

    /** 
     *
     * split les coords, pour chaque appel 
     * createCircle
     * 
     */
    createCircles() {
        const coords = GRAPH.coord.split(" ");

        for (let i = 0; i < coords.length - 1; i++) {
            let [a, b] = coords[i].split(",");  // a => X   b=> Y
            GRAPH.createCircle(a, b, GRAPH.dataValues[i], GRAPH.points[i]);
        }
    },

    /**
     * insert nouveau cercle dans <g>
     * 
     * @param {int} a  point X
     * @param {int} b point Y
     * @param {str} c a mettre dans tooltip pour mouseOver, default ""
     * @param {str} d valeur du point, mettre dans l'egende
     * 
     */
    createCircle(a, b, c = "", d = "") {
        const circle = document.createElementNS(GRAPH.ns, "circle");  // création de la ligne avec NameSpace de la W3C
        circle.setAttribute("cx", a);
        circle.setAttribute("cy", b);
        circle.setAttribute("r", "1");

        // add tooltips element
        const title = document.createElementNS(GRAPH.ns, "title");
        title.innerHTML = c + "\n" + GRAPH.elem + " " + d;
        circle.appendChild(title);

        GRAPH.groupG.appendChild(circle);

    },

    InstanciGroupG() {
        GRAPH.groupG = document.createElementNS(GRAPH.ns, "g");
    },
    
    /**
     * 
     * @param {array} points
     * @param {array} valuesLegende
     * @param {str} elem
     * @param {obj} cadreSVG 
     * @returns {html} element <g> a mettre dans le SVG
     */
    INIT(points, valuesLegende, elem, cadreSVG){
        GRAPH.svgcadre = cadreSVG;
        GRAPH.dataValues = valuesLegende;
        GRAPH.elem = elem;
        GRAPH.points = points;
        GRAPH.InstanciGroupG();
        
        GRAPH.coord = GRAPH.TransformToCoord(points);

        GRAPH.createPolyline();
        GRAPH.createCircles();
        
        return GRAPH.groupG;
    }
};