ADMINISTRATION = {

    toErase: "",
    ladate: "",

    Init() {
        ADMINISTRATION.ladate = document.querySelector('#ladate').textContent;

        document.querySelector("#iconAddUser").addEventListener("click", ADMINISTRATION.toggleAddUser);
        document.querySelector("#closeUser").addEventListener("click", ADMINISTRATION.toggleAddUser);
        
        document.querySelector("#iconAddBuse").addEventListener("click", ADMINISTRATION.toggleAddBuse);
        document.querySelector("#closeBuse").addEventListener("click", ADMINISTRATION.toggleAddBuse);
        
        
        document.querySelectorAll(".userless").forEach(a => a.addEventListener("click", ADMINISTRATION.DellUser));
        document.querySelector("#adduser").addEventListener("click", ADMINISTRATION.AddUser);
        document.querySelector("#addBuse").addEventListener("click", ADMINISTRATION.AddBuse);
        document.querySelectorAll(".rmBuse").forEach(a => a.addEventListener("click", ADMINISTRATION.DellBuse));
    },

    DellUser(e) {
        ADMINISTRATION.toErase = e.target;
        const to = "index.php";
        const what = {
            val: e.target.dataset.info,
            tok: ADMINISTRATION.ladate
        };
        
        const r = confirm("supprimer cette personne ?");
        
        if(r === true){
            AJAX.Post(what, to, "userless=", ADMINISTRATION.retourDellUser);
        }
    },

    retourDellUser(a) {
        if (a === "1") {
            const  grise = ADMINISTRATION.toErase.parentNode;
            grise.parentNode.removeChild(grise);
            ADMINISTRATION.toErase = "";
        } else {
            alert("problème lors de la suppression");
        }
    },

    /**
     * set objet to send with AJAX in Biblio.js
     * @returns {void} call retourAddUser
     */
    AddUser() {
        if (document.querySelector("#name").value !== "" &&
                document.querySelector("#prenom").value !== "" &&
                document.querySelector("#adressemail").value !== "" &&
                document.querySelector("#pass").value === document.querySelector("#pass2").value) {
            let what = {
                nom: document.querySelector("#name").value,
                prenom: document.querySelector("#prenom").value,
                adressemail: document.querySelector("#adressemail").value,
                pass: document.querySelector("#pass").value,
                tok: ADMINISTRATION.ladate
            };
            const to = "index.php";

            AJAX.Post(what, to, "adduser=", ADMINISTRATION.retourAddUser);
        } else {
            alert("remplir tous les champs");
        }
    },
    // TODO  passer de "clone" à "create" au cas ou on n'est plus le premier
    retourAddUser(e) {
        if (e.length === 30) {

            const newDiv = document.querySelector('#listUsers').children[0].cloneNode(true);  //clone la premiere div sans button  child[0]

            //change les valeur pour celles rentrées dans les inputs
            newDiv.querySelector('.bold').innerHTML = `${document.querySelector("#name").value}  ${document.querySelector("#prenom").value}`;
            newDiv.querySelector('.italik').innerHTML = document.querySelector("#adressemail").value;

            //creation button et les attibuts qui vont biens
            const newButton = document.createElement("button");
            newButton.class = "userless";
            newButton.innerHTML = "suppr";
            newButton.setAttribute("data-info", e);
            newButton.addEventListener("click", ADMINISTRATION.DellUser);

            //on ajoute le button au clone
            newDiv.appendChild(newButton);

            // on insert le clone en avant dernier
            document.querySelector('#listUsers').appendChild(newDiv);

            //remise à zero des inputs
            document.querySelector("#name").value = "";
            document.querySelector("#prenom").value = "";
            document.querySelector("#adressemail").value = "";
            document.querySelector("#pass").value = "";
            document.querySelector("#pass2").value = "";
            
            
            //hidde window
            ADMINISTRATION.toggleAddUser();
        } else {
            alert("une erreur c'est produite");
        }
    },

    AddBuse() {
        if (document.querySelector("#nameBuse").value !== "" &&
                document.querySelector("#adresseBuse").value !== "") {
            const  what = {
                nom: document.querySelector("#nameBuse").value,
                adresse: document.querySelector("#adresseBuse").value,
                tok: ADMINISTRATION.ladate
            };
            
            const to = "index.php";
            AJAX.Post(what, to, "newbuse=", ADMINISTRATION.retourAddBuse);
        } else {
            alert("remplir tous les champs");
        }
    },

    retourAddBuse(e) {

        if (e !== "") {
            const newDiv = document.createElement("div");
            newDiv.setAttribute("class", "square");

            const divNomBuse = document.createElement("p");
            divNomBuse.setAttribute("class", "bold bandeau");
            divNomBuse.innerHTML = document.querySelector("#nameBuse").value;

            const divAdresseBuse = document.createElement("p");
            divAdresseBuse.setAttribute("class", "italik");
            divAdresseBuse.innerHTML = "adresse: ".concat(document.querySelector("#adresseBuse").value);

            const buttonBuse = document.createElement("button");
            buttonBuse.innerHTML = "suppr";
            buttonBuse.setAttribute("data-info", e);
            buttonBuse.setAttribute("class", "rmBuse");
            buttonBuse.addEventListener("click", ADMINISTRATION.DellBuse);

            newDiv.appendChild(divNomBuse);
            newDiv.appendChild(divAdresseBuse);
            newDiv.appendChild(buttonBuse);

            document.querySelector('#listModules').appendChild(newDiv);
            
            //hide window
            ADMINISTRATION.toggleAddBuse();
        }
    }, // end retourAddBuse

    DellBuse(e) {
        ADMINISTRATION.toErase = e.target;
        const to = "index.php";
        const what = {
            val: e.target.dataset.info,
            tok: ADMINISTRATION.ladate
            };
        
        const r = confirm("supprimer ce module ?");
        if (r === true){
            AJAX.Post(what, to, "rmBuse=", ADMINISTRATION.retourDellBuse);
        }
    },

    retourDellBuse(a) {
        if (a === "1") {
            const  grise = ADMINISTRATION.toErase.parentNode;
            grise.parentNode.removeChild(grise);
            ADMINISTRATION.toErase = "";
        } else {
            alert("problème lors de la suppression");
        }
    },
    toggleAddUser(){
        document.querySelector("#addUser").classList.toggle('hidden');
    },
    toggleAddBuse(){
        document.querySelector("#ajoutBuse").classList.toggle('hidden');
    }
    
};

ADMINISTRATION.Init();