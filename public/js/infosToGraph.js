IndexIcon = {

    svgcadre: document.querySelector("#graph"), // on récup l'element DOM du <svg>
    elem: "", // elem pour couleur ligne graph, position pour legende

    Init() {
        document.querySelectorAll(".ckickable").forEach(a => a.addEventListener("click", IndexIcon.getSpecifik));
    },

    /**
     *recup valeur de l'element pour le graph of the day
     *
     *  @param {obj} e event clickListener
     */
    getSpecifik(e) {
        const to = "index.php";
        const what = {
            element: e.target.dataset.element,
            buse: e.target.dataset.module
        };
        
        IndexIcon.elem = e.target.dataset.element;

        AJAX.Post(what, to, "graphIndex=", IndexIcon.retourTest);
    },

    /** 
     * gere le retour de getSpecifik via <AJAX> 
     * @param {json} a
     */
    retourTest(a) {
        ab = JSON.parse(a);
        
        const dates = [];
        const values = [];
        ab.forEach(a => {
                    [v,d] = a,
                    dates.push(d),
                    values.push(v);
        });
        const laLigne = GRAPH.INIT(values, dates, IndexIcon.elem, IndexIcon.svgcadre);
        
        IndexIcon.svgcadre.appendChild(laLigne);
    }
};
IndexIcon.Init();