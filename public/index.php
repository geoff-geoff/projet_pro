<?php
    ini_set("session.cookie_httponly", 1);
    session_start();

use App\Autoloader as Autoloader2;
use App\controleur\router;
use Core\Autoloader;

    define("ROOT", dirname(__DIR__).'/');
    require ROOT.'App/autoLoader.php';
    require ROOT.'Core/autoLoader.php';
    require ROOT.'Arduinos/autoLoader.php';
    
    Autoloader2::register(); 
    Autoloader::register();

    $router = new router();

    if (isset($_GET['disconnect'])){
      $router->disconnect();
    }

    if(isset($_SESSION['id'])){
        if($_POST){
            $router->lesPost();
        }else{
            $router->router();  
        }
    }else {
        $router->connection();
    }
?>
